(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory();
	else if(typeof define === 'function' && define.amd)
		define("field-aware-technical", [], factory);
	else if(typeof exports === 'object')
		exports["field-aware-technical"] = factory();
	else
		root["field-aware-technical"] = factory();
})(typeof self !== 'undefined' ? self : this, function() {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/exp/index.js":
/*!**************************!*\
  !*** ./src/exp/index.js ***!
  \**************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/* eslint-disable no-else-return */
var pad = function pad(num, size, floating) {
  var s = num + '';

  if (floating) {
    while (s.length < size) {
      s = '0' + s;
    }
  } else {
    while (s.length < size) {
      s = s + '0';
    }
  }

  ;
  return floating ? '.' + s : s;
};

var Exp =
/*#__PURE__*/
function () {
  /**
   * @param {string} n - A non-negative string representing a number where 1 <= n.length <= 13
   */
  function Exp() {
    var n = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';

    _classCallCheck(this, Exp);

    this._n = '' + n;
  }
  /**
   * @return {string[]}
   */


  _createClass(Exp, [{
    key: "transform",
    value: function transform() {
      if (parseFloat(this._n)) {
        var ret = this._n.split('');

        var toRemove = [];
        var floatIndex = ret.indexOf('.');
        var paddingIndex = floatIndex !== -1 ? floatIndex : ret.length;

        if (floatIndex) {
          ret.splice(paddingIndex, 1);
        }

        ;
        ret.forEach(function (val, i) {
          if (parseFloat(ret[i])) {
            ret[i] = i >= paddingIndex ? pad(val, i + 1 - paddingIndex, true) : pad(val, paddingIndex - i);
          } else {
            toRemove.push(i);
          }
        });
        toRemove.forEach(function (val) {
          ret.splice(val, 1);
        });
        return ret;
      } else {
        return [];
      }
    }
  }, {
    key: "setValue",
    value: function setValue(val) {
      this._n = '' + val;
    }
  }, {
    key: "val",
    get: function get() {
      return this._n;
    }
  }]);

  return Exp;
}();

exports.default = Exp;
module.exports = exports["default"];

/***/ }),

/***/ "./src/index.js":
/*!**********************!*\
  !*** ./src/index.js ***!
  \**********************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "Exp", {
  enumerable: true,
  get: function get() {
    return _exp.default;
  }
});
Object.defineProperty(exports, "Map", {
  enumerable: true,
  get: function get() {
    return _map.default;
  }
});
Object.defineProperty(exports, "Path", {
  enumerable: true,
  get: function get() {
    return _path.default;
  }
});

var _exp = _interopRequireDefault(__webpack_require__(/*! ./exp */ "./src/exp/index.js"));

var _map = _interopRequireDefault(__webpack_require__(/*! ./labyrinth/map.js */ "./src/labyrinth/map.js"));

var _path = _interopRequireDefault(__webpack_require__(/*! ./labyrinth/path.js */ "./src/labyrinth/path.js"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/***/ }),

/***/ "./src/labyrinth/map.js":
/*!******************************!*\
  !*** ./src/labyrinth/map.js ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/* eslint-disable no-else-return */
var _flatten = function _flatten(matrix) {
  var pts = [];

  for (var y = 0; y < matrix.length; y++) {
    for (var x = 0; x < matrix[y].length; x++) {
      pts.push(!matrix[y][x]);
    }
  }

  return pts;
};

var validPoint = function validPoint(pos, dimx, dimy) {
  return pos.x >= 0 && pos.x < dimx && pos.y >= 0 && pos.y < dimy;
};

var Map =
/*#__PURE__*/
function () {
  /**
  * @param {Array[]} matrix Matrix Table, defaults to [[0]], if argument is not provided
  * @param {Array} matrix[] A row within the matrix table
  * @param {number} matrix[][] a cell within a row, represented by either 1 or 0
  */
  function Map() {
    var matrix = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [[0]];

    _classCallCheck(this, Map);

    this._dimx = matrix[0].length;
    this._dimy = matrix.length;
    this._points = _flatten(matrix);
  }
  /**
   * points getter
   * @return {Array[boolean]}
   */


  _createClass(Map, [{
    key: "flatten",
    value: function flatten(matrix) {
      return _flatten(matrix);
    }
    /**
     * @param {number} pos
     * @param {boolean} sticky
     */

  }, {
    key: "getPoint",
    value: function getPoint(pos, sticky) {
      var x = pos.x,
          y = pos.y;

      if (!validPoint(pos, this._dimx, this._dimy)) {
        return false;
      } else {
        var r = this._points[x * this._dimx + y];
        this.setPoint(pos, sticky);
        return r;
      }
    }
    /**
     * @param {*} pos
     * @param {*} value
     */

  }, {
    key: "setPoint",
    value: function setPoint(pos, value) {
      var x = pos.x,
          y = pos.y;
      if (typeof value !== 'undefined') this._points[x * this._dimx + y] = value;
      return this._points[x * this._dimx + y];
    }
  }, {
    key: "points",
    get: function get() {
      return this._points;
    },
    set: function set(matrix) {
      this._points = _flatten(matrix);
    }
  }]);

  return Map;
}();

exports.default = Map;
module.exports = exports["default"];

/***/ }),

/***/ "./src/labyrinth/path.js":
/*!*******************************!*\
  !*** ./src/labyrinth/path.js ***!
  \*******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _map = _interopRequireDefault(__webpack_require__(/*! ./map.js */ "./src/labyrinth/map.js"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var Point = function Point(x, y) {
  return {
    x: x,
    y: y
  };
};

var getFutures = function getFutures(crrts, map) {
  var nexts = [];

  var getAdjacents = function getAdjacents(pos) {
    var x = pos.x,
        y = pos.y;
    return [Point(x - 1, y), Point(x + 1, y), Point(x, y - 1), Point(x, y + 1)];
  };

  for (var i = 0; i < crrts.length; i++) {
    var adjs = getAdjacents(crrts[i]);

    for (var j = 0; j < adjs.length; j++) {
      if (map.getPoint(adjs[j], false)) {
        nexts.push(adjs[j]);
      }
    }
  }

  return nexts;
};

var Path =
/*#__PURE__*/
function (_Map) {
  _inherits(Path, _Map);

  /**
   * @param {Array[]} matrix Matrix Table, defaults to [[0]], if argument is not provided
   * @param {number[]} matrix[] A row within the matrix table
   * @param {number} matrix[][] a cell within a row, represented by either 1 or 0
   */
  function Path() {
    var _this;

    var matrix = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [[0]];

    _classCallCheck(this, Path);

    _this = _possibleConstructorReturn(this, (Path.__proto__ || Object.getPrototypeOf(Path)).call(this, matrix));
    _this._start = {
      x: 0,
      y: 0
    };
    _this._goal = {
      x: matrix[0].length - 1,
      y: matrix.length - 1
    };
    return _this;
  }
  /**
   * Getters
   */


  _createClass(Path, [{
    key: "exists",

    /**
     * @return {boolean}
     */
    value: function exists() {
      var futures = [this.start];

      do {
        futures = getFutures(futures, this);

        for (var i = 0; i < futures.length; i++) {
          var future = futures[i];
          if (future.x === this._goal.x && future.y === this._goal.y) return true;
        }
      } while (futures.length !== 0);

      return false;
    }
  }, {
    key: "start",
    get: function get() {
      return this._start;
    }
  }, {
    key: "goal",
    get: function get() {
      return this._goal;
    }
  }]);

  return Path;
}(_map.default);

exports.default = Path;
module.exports = exports["default"];

/***/ })

/******/ });
});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9maWVsZC1hd2FyZS10ZWNobmljYWwvd2VicGFjay91bml2ZXJzYWxNb2R1bGVEZWZpbml0aW9uIiwid2VicGFjazovL2ZpZWxkLWF3YXJlLXRlY2huaWNhbC93ZWJwYWNrL2Jvb3RzdHJhcCIsIndlYnBhY2s6Ly9maWVsZC1hd2FyZS10ZWNobmljYWwvLi9zcmMvZXhwL2luZGV4LmpzIiwid2VicGFjazovL2ZpZWxkLWF3YXJlLXRlY2huaWNhbC8uL3NyYy9pbmRleC5qcyIsIndlYnBhY2s6Ly9maWVsZC1hd2FyZS10ZWNobmljYWwvLi9zcmMvbGFieXJpbnRoL21hcC5qcyIsIndlYnBhY2s6Ly9maWVsZC1hd2FyZS10ZWNobmljYWwvLi9zcmMvbGFieXJpbnRoL3BhdGguanMiXSwibmFtZXMiOlsicGFkIiwibnVtIiwic2l6ZSIsImZsb2F0aW5nIiwicyIsImxlbmd0aCIsIkV4cCIsIm4iLCJfbiIsInBhcnNlRmxvYXQiLCJyZXQiLCJzcGxpdCIsInRvUmVtb3ZlIiwiZmxvYXRJbmRleCIsImluZGV4T2YiLCJwYWRkaW5nSW5kZXgiLCJzcGxpY2UiLCJmb3JFYWNoIiwidmFsIiwiaSIsInB1c2giLCJfZmxhdHRlbiIsIm1hdHJpeCIsInB0cyIsInkiLCJ4IiwidmFsaWRQb2ludCIsInBvcyIsImRpbXgiLCJkaW15IiwiTWFwIiwiX2RpbXgiLCJfZGlteSIsIl9wb2ludHMiLCJzdGlja3kiLCJyIiwic2V0UG9pbnQiLCJ2YWx1ZSIsIlBvaW50IiwiZ2V0RnV0dXJlcyIsImNycnRzIiwibWFwIiwibmV4dHMiLCJnZXRBZGphY2VudHMiLCJhZGpzIiwiaiIsImdldFBvaW50IiwiUGF0aCIsIl9zdGFydCIsIl9nb2FsIiwiZnV0dXJlcyIsInN0YXJ0IiwiZnV0dXJlIl0sIm1hcHBpbmdzIjoiQUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDQUFDO0FBQ0QsTztBQ1ZBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOzs7QUFHQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0Esa0RBQTBDLGdDQUFnQztBQUMxRTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLGdFQUF3RCxrQkFBa0I7QUFDMUU7QUFDQSx5REFBaUQsY0FBYztBQUMvRDs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsaURBQXlDLGlDQUFpQztBQUMxRSx3SEFBZ0gsbUJBQW1CLEVBQUU7QUFDckk7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxtQ0FBMkIsMEJBQTBCLEVBQUU7QUFDdkQseUNBQWlDLGVBQWU7QUFDaEQ7QUFDQTtBQUNBOztBQUVBO0FBQ0EsOERBQXNELCtEQUErRDs7QUFFckg7QUFDQTs7O0FBR0E7QUFDQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNsRkE7QUFFQSxJQUFJQSxHQUFHLEdBQUcsU0FBTkEsR0FBTSxDQUFDQyxHQUFELEVBQU1DLElBQU4sRUFBWUMsUUFBWixFQUF5QjtBQUNqQyxNQUFJQyxDQUFDLEdBQUdILEdBQUcsR0FBRyxFQUFkOztBQUVBLE1BQUlFLFFBQUosRUFBYztBQUNaLFdBQU9DLENBQUMsQ0FBQ0MsTUFBRixHQUFXSCxJQUFsQjtBQUF3QkUsT0FBQyxHQUFHLE1BQU1BLENBQVY7QUFBeEI7QUFDRCxHQUZELE1BRU87QUFDTCxXQUFPQSxDQUFDLENBQUNDLE1BQUYsR0FBV0gsSUFBbEI7QUFBd0JFLE9BQUMsR0FBR0EsQ0FBQyxHQUFHLEdBQVI7QUFBeEI7QUFDRDs7QUFBQTtBQUNELFNBQU9ELFFBQVEsR0FBRyxNQUFNQyxDQUFULEdBQWFBLENBQTVCO0FBQ0QsQ0FURDs7SUFXcUJFLEc7OztBQUVuQjs7O0FBR0EsaUJBQW9CO0FBQUEsUUFBUkMsQ0FBUSx1RUFBSixFQUFJOztBQUFBOztBQUNsQixTQUFLQyxFQUFMLEdBQVUsS0FBS0QsQ0FBZjtBQUNEO0FBRUQ7Ozs7Ozs7Z0NBR1k7QUFDVixVQUFJRSxVQUFVLENBQUMsS0FBS0QsRUFBTixDQUFkLEVBQXlCO0FBQ3ZCLFlBQUlFLEdBQUcsR0FBRyxLQUFLRixFQUFMLENBQVFHLEtBQVIsQ0FBYyxFQUFkLENBQVY7O0FBQ0EsWUFBSUMsUUFBUSxHQUFHLEVBQWY7QUFDQSxZQUFNQyxVQUFVLEdBQUdILEdBQUcsQ0FBQ0ksT0FBSixDQUFZLEdBQVosQ0FBbkI7QUFDQSxZQUFNQyxZQUFZLEdBQUdGLFVBQVUsS0FBSyxDQUFDLENBQWhCLEdBQW9CQSxVQUFwQixHQUFpQ0gsR0FBRyxDQUFDTCxNQUExRDs7QUFFQSxZQUFJUSxVQUFKLEVBQWdCO0FBQ2RILGFBQUcsQ0FBQ00sTUFBSixDQUFXRCxZQUFYLEVBQXlCLENBQXpCO0FBQ0Q7O0FBQUE7QUFFREwsV0FBRyxDQUFDTyxPQUFKLENBQVksVUFBQ0MsR0FBRCxFQUFNQyxDQUFOLEVBQVk7QUFDdEIsY0FBSVYsVUFBVSxDQUFDQyxHQUFHLENBQUNTLENBQUQsQ0FBSixDQUFkLEVBQXdCO0FBQ3RCVCxlQUFHLENBQUNTLENBQUQsQ0FBSCxHQUFTQSxDQUFDLElBQUlKLFlBQUwsR0FBb0JmLEdBQUcsQ0FBQ2tCLEdBQUQsRUFBUUMsQ0FBQyxHQUFHLENBQUwsR0FBVUosWUFBakIsRUFBZ0MsSUFBaEMsQ0FBdkIsR0FBK0RmLEdBQUcsQ0FBQ2tCLEdBQUQsRUFBT0gsWUFBWSxHQUFHSSxDQUF0QixDQUEzRTtBQUNELFdBRkQsTUFFTztBQUNMUCxvQkFBUSxDQUFDUSxJQUFULENBQWNELENBQWQ7QUFDRDtBQUNGLFNBTkQ7QUFRQVAsZ0JBQVEsQ0FBQ0ssT0FBVCxDQUFpQixVQUFDQyxHQUFELEVBQVM7QUFDeEJSLGFBQUcsQ0FBQ00sTUFBSixDQUFXRSxHQUFYLEVBQWdCLENBQWhCO0FBQ0QsU0FGRDtBQUlBLGVBQU9SLEdBQVA7QUFDRCxPQXZCRCxNQXVCTztBQUNMLGVBQU8sRUFBUDtBQUNEO0FBQ0Y7Ozs2QkFNUVEsRyxFQUFLO0FBQ1osV0FBS1YsRUFBTCxHQUFVLEtBQUtVLEdBQWY7QUFDRDs7O3dCQU5TO0FBQ1IsYUFBTyxLQUFLVixFQUFaO0FBQ0Q7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUN4REg7O0FBQ0E7O0FBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ0ZBO0FBRUEsSUFBSWEsUUFBUSxHQUFHLFNBQVhBLFFBQVcsQ0FBQ0MsTUFBRCxFQUFZO0FBQ3pCLE1BQUlDLEdBQUcsR0FBRyxFQUFWOztBQUVBLE9BQUssSUFBSUMsQ0FBQyxHQUFHLENBQWIsRUFBZ0JBLENBQUMsR0FBR0YsTUFBTSxDQUFDakIsTUFBM0IsRUFBbUNtQixDQUFDLEVBQXBDLEVBQXdDO0FBQ3RDLFNBQUssSUFBSUMsQ0FBQyxHQUFHLENBQWIsRUFBZ0JBLENBQUMsR0FBR0gsTUFBTSxDQUFDRSxDQUFELENBQU4sQ0FBVW5CLE1BQTlCLEVBQXNDb0IsQ0FBQyxFQUF2QyxFQUEyQztBQUN6Q0YsU0FBRyxDQUFDSCxJQUFKLENBQVMsQ0FBQ0UsTUFBTSxDQUFDRSxDQUFELENBQU4sQ0FBVUMsQ0FBVixDQUFWO0FBQ0Q7QUFDRjs7QUFDRCxTQUFPRixHQUFQO0FBQ0QsQ0FURDs7QUFXQSxJQUFJRyxVQUFVLEdBQUcsU0FBYkEsVUFBYSxDQUFDQyxHQUFELEVBQU1DLElBQU4sRUFBWUMsSUFBWixFQUFxQjtBQUNwQyxTQUFRRixHQUFHLENBQUNGLENBQUosSUFBUyxDQUFWLElBQWlCRSxHQUFHLENBQUNGLENBQUosR0FBUUcsSUFBekIsSUFBbUNELEdBQUcsQ0FBQ0gsQ0FBSixJQUFTLENBQTVDLElBQW1ERyxHQUFHLENBQUNILENBQUosR0FBUUssSUFBbEU7QUFDRCxDQUZEOztJQUlxQkMsRzs7O0FBRW5COzs7OztBQUtBLGlCQUE0QjtBQUFBLFFBQWhCUixNQUFnQix1RUFBUCxDQUFDLENBQUMsQ0FBRCxDQUFELENBQU87O0FBQUE7O0FBQzFCLFNBQUtTLEtBQUwsR0FBYVQsTUFBTSxDQUFDLENBQUQsQ0FBTixDQUFVakIsTUFBdkI7QUFDQSxTQUFLMkIsS0FBTCxHQUFhVixNQUFNLENBQUNqQixNQUFwQjtBQUNBLFNBQUs0QixPQUFMLEdBQWVaLFFBQVEsQ0FBQ0MsTUFBRCxDQUF2QjtBQUNEO0FBRUQ7Ozs7Ozs7OzRCQVlRQSxNLEVBQVE7QUFDZCxhQUFPRCxRQUFRLENBQUNDLE1BQUQsQ0FBZjtBQUNEO0FBQ0Q7Ozs7Ozs7NkJBSVNLLEcsRUFBS08sTSxFQUFRO0FBQ3BCLFVBQUlULENBQUMsR0FBR0UsR0FBRyxDQUFDRixDQUFaO0FBQUEsVUFDRUQsQ0FBQyxHQUFHRyxHQUFHLENBQUNILENBRFY7O0FBR0EsVUFBSSxDQUFDRSxVQUFVLENBQUNDLEdBQUQsRUFBTSxLQUFLSSxLQUFYLEVBQWtCLEtBQUtDLEtBQXZCLENBQWYsRUFBOEM7QUFDNUMsZUFBTyxLQUFQO0FBQ0QsT0FGRCxNQUVPO0FBQ0wsWUFBSUcsQ0FBQyxHQUFHLEtBQUtGLE9BQUwsQ0FBYVIsQ0FBQyxHQUFHLEtBQUtNLEtBQVQsR0FBaUJQLENBQTlCLENBQVI7QUFFQSxhQUFLWSxRQUFMLENBQWNULEdBQWQsRUFBbUJPLE1BQW5CO0FBRUEsZUFBT0MsQ0FBUDtBQUNEO0FBQ0Y7QUFFRDs7Ozs7Ozs2QkFJU1IsRyxFQUFLVSxLLEVBQU87QUFDbkIsVUFBSVosQ0FBQyxHQUFHRSxHQUFHLENBQUNGLENBQVo7QUFBQSxVQUNFRCxDQUFDLEdBQUdHLEdBQUcsQ0FBQ0gsQ0FEVjtBQUdBLFVBQUksT0FBT2EsS0FBUCxLQUFpQixXQUFyQixFQUFrQyxLQUFLSixPQUFMLENBQWFSLENBQUMsR0FBRyxLQUFLTSxLQUFULEdBQWlCUCxDQUE5QixJQUFtQ2EsS0FBbkM7QUFFbEMsYUFBTyxLQUFLSixPQUFMLENBQWFSLENBQUMsR0FBRyxLQUFLTSxLQUFULEdBQWlCUCxDQUE5QixDQUFQO0FBQ0Q7Ozt3QkF6Q1k7QUFDWCxhQUFPLEtBQUtTLE9BQVo7QUFDRCxLO3NCQUVVWCxNLEVBQVE7QUFDakIsV0FBS1csT0FBTCxHQUFlWixRQUFRLENBQUNDLE1BQUQsQ0FBdkI7QUFDRDs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUN4Q0g7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBRUEsSUFBSWdCLEtBQUssR0FBRyxTQUFSQSxLQUFRLENBQUNiLENBQUQsRUFBSUQsQ0FBSixFQUFVO0FBQ3BCLFNBQU87QUFDTEMsS0FBQyxFQUFFQSxDQURFO0FBRUxELEtBQUMsRUFBRUE7QUFGRSxHQUFQO0FBSUQsQ0FMRDs7QUFPQSxJQUFJZSxVQUFVLEdBQUcsU0FBYkEsVUFBYSxDQUFDQyxLQUFELEVBQVFDLEdBQVIsRUFBZ0I7QUFFL0IsTUFBSUMsS0FBSyxHQUFHLEVBQVo7O0FBRUEsTUFBTUMsWUFBWSxHQUFHLFNBQWZBLFlBQWUsQ0FBQ2hCLEdBQUQsRUFBUztBQUM1QixRQUFNRixDQUFDLEdBQUdFLEdBQUcsQ0FBQ0YsQ0FBZDtBQUFBLFFBQ0VELENBQUMsR0FBR0csR0FBRyxDQUFDSCxDQURWO0FBR0EsV0FBTyxDQUNMYyxLQUFLLENBQUNiLENBQUMsR0FBRyxDQUFMLEVBQVFELENBQVIsQ0FEQSxFQUNZYyxLQUFLLENBQUNiLENBQUMsR0FBRyxDQUFMLEVBQVFELENBQVIsQ0FEakIsRUFFTGMsS0FBSyxDQUFDYixDQUFELEVBQUlELENBQUMsR0FBRyxDQUFSLENBRkEsRUFFWWMsS0FBSyxDQUFDYixDQUFELEVBQUlELENBQUMsR0FBRyxDQUFSLENBRmpCLENBQVA7QUFJRCxHQVJEOztBQVVBLE9BQUssSUFBSUwsQ0FBQyxHQUFHLENBQWIsRUFBZ0JBLENBQUMsR0FBR3FCLEtBQUssQ0FBQ25DLE1BQTFCLEVBQWtDYyxDQUFDLEVBQW5DLEVBQXVDO0FBQ3JDLFFBQUl5QixJQUFJLEdBQUdELFlBQVksQ0FBQ0gsS0FBSyxDQUFDckIsQ0FBRCxDQUFOLENBQXZCOztBQUVBLFNBQUssSUFBSTBCLENBQUMsR0FBRyxDQUFiLEVBQWdCQSxDQUFDLEdBQUdELElBQUksQ0FBQ3ZDLE1BQXpCLEVBQWlDd0MsQ0FBQyxFQUFsQyxFQUFzQztBQUNwQyxVQUFJSixHQUFHLENBQUNLLFFBQUosQ0FBYUYsSUFBSSxDQUFDQyxDQUFELENBQWpCLEVBQXNCLEtBQXRCLENBQUosRUFBa0M7QUFDaENILGFBQUssQ0FBQ3RCLElBQU4sQ0FBV3dCLElBQUksQ0FBQ0MsQ0FBRCxDQUFmO0FBQ0Q7QUFDRjtBQUNGOztBQUNELFNBQU9ILEtBQVA7QUFDRCxDQXhCRDs7SUEwQnFCSyxJOzs7OztBQUVuQjs7Ozs7QUFLQSxrQkFBNEI7QUFBQTs7QUFBQSxRQUFoQnpCLE1BQWdCLHVFQUFQLENBQUMsQ0FBQyxDQUFELENBQUQsQ0FBTzs7QUFBQTs7QUFDMUIsd0dBQU1BLE1BQU47QUFFQSxVQUFLMEIsTUFBTCxHQUFjO0FBQ1p2QixPQUFDLEVBQUUsQ0FEUztBQUVaRCxPQUFDLEVBQUU7QUFGUyxLQUFkO0FBSUEsVUFBS3lCLEtBQUwsR0FBYTtBQUNYeEIsT0FBQyxFQUFFSCxNQUFNLENBQUMsQ0FBRCxDQUFOLENBQVVqQixNQUFWLEdBQW1CLENBRFg7QUFFWG1CLE9BQUMsRUFBRUYsTUFBTSxDQUFDakIsTUFBUCxHQUFnQjtBQUZSLEtBQWI7QUFQMEI7QUFXM0I7QUFFRDs7Ozs7Ozs7QUFXQTs7OzZCQUdTO0FBQ1AsVUFBSTZDLE9BQU8sR0FBRyxDQUFDLEtBQUtDLEtBQU4sQ0FBZDs7QUFFQSxTQUFHO0FBQ0RELGVBQU8sR0FBR1gsVUFBVSxDQUFDVyxPQUFELEVBQVUsSUFBVixDQUFwQjs7QUFFQSxhQUFLLElBQUkvQixDQUFDLEdBQUcsQ0FBYixFQUFnQkEsQ0FBQyxHQUFHK0IsT0FBTyxDQUFDN0MsTUFBNUIsRUFBb0NjLENBQUMsRUFBckMsRUFBeUM7QUFDdkMsY0FBSWlDLE1BQU0sR0FBR0YsT0FBTyxDQUFDL0IsQ0FBRCxDQUFwQjtBQUVBLGNBQUtpQyxNQUFNLENBQUMzQixDQUFQLEtBQWEsS0FBS3dCLEtBQUwsQ0FBV3hCLENBQXpCLElBQWdDMkIsTUFBTSxDQUFDNUIsQ0FBUCxLQUFhLEtBQUt5QixLQUFMLENBQVd6QixDQUE1RCxFQUFnRSxPQUFPLElBQVA7QUFDakU7QUFDRixPQVJELFFBUVMwQixPQUFPLENBQUM3QyxNQUFSLEtBQW1CLENBUjVCOztBQVNBLGFBQU8sS0FBUDtBQUNEOzs7d0JBeEJXO0FBQ1YsYUFBTyxLQUFLMkMsTUFBWjtBQUNEOzs7d0JBRVU7QUFDVCxhQUFPLEtBQUtDLEtBQVo7QUFDRCIsImZpbGUiOiJmaWVsZC1hd2FyZS10ZWNobmljYWwuanMiLCJzb3VyY2VzQ29udGVudCI6WyIoZnVuY3Rpb24gd2VicGFja1VuaXZlcnNhbE1vZHVsZURlZmluaXRpb24ocm9vdCwgZmFjdG9yeSkge1xuXHRpZih0eXBlb2YgZXhwb3J0cyA9PT0gJ29iamVjdCcgJiYgdHlwZW9mIG1vZHVsZSA9PT0gJ29iamVjdCcpXG5cdFx0bW9kdWxlLmV4cG9ydHMgPSBmYWN0b3J5KCk7XG5cdGVsc2UgaWYodHlwZW9mIGRlZmluZSA9PT0gJ2Z1bmN0aW9uJyAmJiBkZWZpbmUuYW1kKVxuXHRcdGRlZmluZShcImZpZWxkLWF3YXJlLXRlY2huaWNhbFwiLCBbXSwgZmFjdG9yeSk7XG5cdGVsc2UgaWYodHlwZW9mIGV4cG9ydHMgPT09ICdvYmplY3QnKVxuXHRcdGV4cG9ydHNbXCJmaWVsZC1hd2FyZS10ZWNobmljYWxcIl0gPSBmYWN0b3J5KCk7XG5cdGVsc2Vcblx0XHRyb290W1wiZmllbGQtYXdhcmUtdGVjaG5pY2FsXCJdID0gZmFjdG9yeSgpO1xufSkodHlwZW9mIHNlbGYgIT09ICd1bmRlZmluZWQnID8gc2VsZiA6IHRoaXMsIGZ1bmN0aW9uKCkge1xucmV0dXJuICIsIiBcdC8vIFRoZSBtb2R1bGUgY2FjaGVcbiBcdHZhciBpbnN0YWxsZWRNb2R1bGVzID0ge307XG5cbiBcdC8vIFRoZSByZXF1aXJlIGZ1bmN0aW9uXG4gXHRmdW5jdGlvbiBfX3dlYnBhY2tfcmVxdWlyZV9fKG1vZHVsZUlkKSB7XG5cbiBcdFx0Ly8gQ2hlY2sgaWYgbW9kdWxlIGlzIGluIGNhY2hlXG4gXHRcdGlmKGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdKSB7XG4gXHRcdFx0cmV0dXJuIGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdLmV4cG9ydHM7XG4gXHRcdH1cbiBcdFx0Ly8gQ3JlYXRlIGEgbmV3IG1vZHVsZSAoYW5kIHB1dCBpdCBpbnRvIHRoZSBjYWNoZSlcbiBcdFx0dmFyIG1vZHVsZSA9IGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdID0ge1xuIFx0XHRcdGk6IG1vZHVsZUlkLFxuIFx0XHRcdGw6IGZhbHNlLFxuIFx0XHRcdGV4cG9ydHM6IHt9XG4gXHRcdH07XG5cbiBcdFx0Ly8gRXhlY3V0ZSB0aGUgbW9kdWxlIGZ1bmN0aW9uXG4gXHRcdG1vZHVsZXNbbW9kdWxlSWRdLmNhbGwobW9kdWxlLmV4cG9ydHMsIG1vZHVsZSwgbW9kdWxlLmV4cG9ydHMsIF9fd2VicGFja19yZXF1aXJlX18pO1xuXG4gXHRcdC8vIEZsYWcgdGhlIG1vZHVsZSBhcyBsb2FkZWRcbiBcdFx0bW9kdWxlLmwgPSB0cnVlO1xuXG4gXHRcdC8vIFJldHVybiB0aGUgZXhwb3J0cyBvZiB0aGUgbW9kdWxlXG4gXHRcdHJldHVybiBtb2R1bGUuZXhwb3J0cztcbiBcdH1cblxuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZXMgb2JqZWN0IChfX3dlYnBhY2tfbW9kdWxlc19fKVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5tID0gbW9kdWxlcztcblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGUgY2FjaGVcbiBcdF9fd2VicGFja19yZXF1aXJlX18uYyA9IGluc3RhbGxlZE1vZHVsZXM7XG5cbiBcdC8vIGRlZmluZSBnZXR0ZXIgZnVuY3Rpb24gZm9yIGhhcm1vbnkgZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kID0gZnVuY3Rpb24oZXhwb3J0cywgbmFtZSwgZ2V0dGVyKSB7XG4gXHRcdGlmKCFfX3dlYnBhY2tfcmVxdWlyZV9fLm8oZXhwb3J0cywgbmFtZSkpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgbmFtZSwgeyBlbnVtZXJhYmxlOiB0cnVlLCBnZXQ6IGdldHRlciB9KTtcbiBcdFx0fVxuIFx0fTtcblxuIFx0Ly8gZGVmaW5lIF9fZXNNb2R1bGUgb24gZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5yID0gZnVuY3Rpb24oZXhwb3J0cykge1xuIFx0XHRpZih0eXBlb2YgU3ltYm9sICE9PSAndW5kZWZpbmVkJyAmJiBTeW1ib2wudG9TdHJpbmdUYWcpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgU3ltYm9sLnRvU3RyaW5nVGFnLCB7IHZhbHVlOiAnTW9kdWxlJyB9KTtcbiBcdFx0fVxuIFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgJ19fZXNNb2R1bGUnLCB7IHZhbHVlOiB0cnVlIH0pO1xuIFx0fTtcblxuIFx0Ly8gY3JlYXRlIGEgZmFrZSBuYW1lc3BhY2Ugb2JqZWN0XG4gXHQvLyBtb2RlICYgMTogdmFsdWUgaXMgYSBtb2R1bGUgaWQsIHJlcXVpcmUgaXRcbiBcdC8vIG1vZGUgJiAyOiBtZXJnZSBhbGwgcHJvcGVydGllcyBvZiB2YWx1ZSBpbnRvIHRoZSBuc1xuIFx0Ly8gbW9kZSAmIDQ6IHJldHVybiB2YWx1ZSB3aGVuIGFscmVhZHkgbnMgb2JqZWN0XG4gXHQvLyBtb2RlICYgOHwxOiBiZWhhdmUgbGlrZSByZXF1aXJlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnQgPSBmdW5jdGlvbih2YWx1ZSwgbW9kZSkge1xuIFx0XHRpZihtb2RlICYgMSkgdmFsdWUgPSBfX3dlYnBhY2tfcmVxdWlyZV9fKHZhbHVlKTtcbiBcdFx0aWYobW9kZSAmIDgpIHJldHVybiB2YWx1ZTtcbiBcdFx0aWYoKG1vZGUgJiA0KSAmJiB0eXBlb2YgdmFsdWUgPT09ICdvYmplY3QnICYmIHZhbHVlICYmIHZhbHVlLl9fZXNNb2R1bGUpIHJldHVybiB2YWx1ZTtcbiBcdFx0dmFyIG5zID0gT2JqZWN0LmNyZWF0ZShudWxsKTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5yKG5zKTtcbiBcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KG5zLCAnZGVmYXVsdCcsIHsgZW51bWVyYWJsZTogdHJ1ZSwgdmFsdWU6IHZhbHVlIH0pO1xuIFx0XHRpZihtb2RlICYgMiAmJiB0eXBlb2YgdmFsdWUgIT0gJ3N0cmluZycpIGZvcih2YXIga2V5IGluIHZhbHVlKSBfX3dlYnBhY2tfcmVxdWlyZV9fLmQobnMsIGtleSwgZnVuY3Rpb24oa2V5KSB7IHJldHVybiB2YWx1ZVtrZXldOyB9LmJpbmQobnVsbCwga2V5KSk7XG4gXHRcdHJldHVybiBucztcbiBcdH07XG5cbiBcdC8vIGdldERlZmF1bHRFeHBvcnQgZnVuY3Rpb24gZm9yIGNvbXBhdGliaWxpdHkgd2l0aCBub24taGFybW9ueSBtb2R1bGVzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm4gPSBmdW5jdGlvbihtb2R1bGUpIHtcbiBcdFx0dmFyIGdldHRlciA9IG1vZHVsZSAmJiBtb2R1bGUuX19lc01vZHVsZSA/XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0RGVmYXVsdCgpIHsgcmV0dXJuIG1vZHVsZVsnZGVmYXVsdCddOyB9IDpcbiBcdFx0XHRmdW5jdGlvbiBnZXRNb2R1bGVFeHBvcnRzKCkgeyByZXR1cm4gbW9kdWxlOyB9O1xuIFx0XHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQoZ2V0dGVyLCAnYScsIGdldHRlcik7XG4gXHRcdHJldHVybiBnZXR0ZXI7XG4gXHR9O1xuXG4gXHQvLyBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGxcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubyA9IGZ1bmN0aW9uKG9iamVjdCwgcHJvcGVydHkpIHsgcmV0dXJuIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChvYmplY3QsIHByb3BlcnR5KTsgfTtcblxuIFx0Ly8gX193ZWJwYWNrX3B1YmxpY19wYXRoX19cbiBcdF9fd2VicGFja19yZXF1aXJlX18ucCA9IFwiXCI7XG5cblxuIFx0Ly8gTG9hZCBlbnRyeSBtb2R1bGUgYW5kIHJldHVybiBleHBvcnRzXG4gXHRyZXR1cm4gX193ZWJwYWNrX3JlcXVpcmVfXyhfX3dlYnBhY2tfcmVxdWlyZV9fLnMgPSBcIi4vc3JjL2luZGV4LmpzXCIpO1xuIiwiLyogZXNsaW50LWRpc2FibGUgbm8tZWxzZS1yZXR1cm4gKi9cblxubGV0IHBhZCA9IChudW0sIHNpemUsIGZsb2F0aW5nKSA9PiB7XG4gIGxldCBzID0gbnVtICsgJyc7XG5cbiAgaWYgKGZsb2F0aW5nKSB7XG4gICAgd2hpbGUgKHMubGVuZ3RoIDwgc2l6ZSkgcyA9ICcwJyArIHM7XG4gIH0gZWxzZSB7XG4gICAgd2hpbGUgKHMubGVuZ3RoIDwgc2l6ZSkgcyA9IHMgKyAnMCc7XG4gIH07XG4gIHJldHVybiBmbG9hdGluZyA/ICcuJyArIHMgOiBzO1xufTtcblxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgRXhwIHtcblxuICAvKipcbiAgICogQHBhcmFtIHtzdHJpbmd9IG4gLSBBIG5vbi1uZWdhdGl2ZSBzdHJpbmcgcmVwcmVzZW50aW5nIGEgbnVtYmVyIHdoZXJlIDEgPD0gbi5sZW5ndGggPD0gMTNcbiAgICovXG4gIGNvbnN0cnVjdG9yKG4gPSAnJykge1xuICAgIHRoaXMuX24gPSAnJyArIG47XG4gIH1cblxuICAvKipcbiAgICogQHJldHVybiB7c3RyaW5nW119XG4gICAqL1xuICB0cmFuc2Zvcm0oKSB7XG4gICAgaWYgKHBhcnNlRmxvYXQodGhpcy5fbikpIHtcbiAgICAgIGxldCByZXQgPSB0aGlzLl9uLnNwbGl0KCcnKTtcbiAgICAgIGxldCB0b1JlbW92ZSA9IFtdO1xuICAgICAgY29uc3QgZmxvYXRJbmRleCA9IHJldC5pbmRleE9mKCcuJyk7XG4gICAgICBjb25zdCBwYWRkaW5nSW5kZXggPSBmbG9hdEluZGV4ICE9PSAtMSA/IGZsb2F0SW5kZXggOiByZXQubGVuZ3RoO1xuXG4gICAgICBpZiAoZmxvYXRJbmRleCkge1xuICAgICAgICByZXQuc3BsaWNlKHBhZGRpbmdJbmRleCwgMSk7XG4gICAgICB9O1xuXG4gICAgICByZXQuZm9yRWFjaCgodmFsLCBpKSA9PiB7XG4gICAgICAgIGlmIChwYXJzZUZsb2F0KHJldFtpXSkpIHtcbiAgICAgICAgICByZXRbaV0gPSBpID49IHBhZGRpbmdJbmRleCA/IHBhZCh2YWwsICgoaSArIDEpIC0gcGFkZGluZ0luZGV4KSwgdHJ1ZSkgOiBwYWQodmFsLCAocGFkZGluZ0luZGV4IC0gaSkpO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIHRvUmVtb3ZlLnB1c2goaSk7XG4gICAgICAgIH1cbiAgICAgIH0pO1xuXG4gICAgICB0b1JlbW92ZS5mb3JFYWNoKCh2YWwpID0+IHtcbiAgICAgICAgcmV0LnNwbGljZSh2YWwsIDEpO1xuICAgICAgfSk7XG5cbiAgICAgIHJldHVybiByZXQ7XG4gICAgfSBlbHNlIHtcbiAgICAgIHJldHVybiBbXTtcbiAgICB9XG4gIH1cblxuICBnZXQgdmFsKCkge1xuICAgIHJldHVybiB0aGlzLl9uO1xuICB9XG5cbiAgc2V0VmFsdWUodmFsKSB7XG4gICAgdGhpcy5fbiA9ICcnICsgdmFsO1xuICB9XG5cbn1cbiIsImltcG9ydCBFeHAgZnJvbSAnLi9leHAnO1xuaW1wb3J0IE1hcCBmcm9tICcuL2xhYnlyaW50aC9tYXAuanMnO1xuaW1wb3J0IFBhdGggZnJvbSAnLi9sYWJ5cmludGgvcGF0aC5qcyc7XG5cbmV4cG9ydCB7IEV4cCwgTWFwLCBQYXRoIH07XG4iLCIvKiBlc2xpbnQtZGlzYWJsZSBuby1lbHNlLXJldHVybiAqL1xuXG5sZXQgX2ZsYXR0ZW4gPSAobWF0cml4KSA9PiB7XG4gIGxldCBwdHMgPSBbXTtcblxuICBmb3IgKGxldCB5ID0gMDsgeSA8IG1hdHJpeC5sZW5ndGg7IHkrKykge1xuICAgIGZvciAobGV0IHggPSAwOyB4IDwgbWF0cml4W3ldLmxlbmd0aDsgeCsrKSB7XG4gICAgICBwdHMucHVzaCghbWF0cml4W3ldW3hdKTtcbiAgICB9XG4gIH1cbiAgcmV0dXJuIHB0cztcbn07XG5cbmxldCB2YWxpZFBvaW50ID0gKHBvcywgZGlteCwgZGlteSkgPT4ge1xuICByZXR1cm4gKHBvcy54ID49IDApICYmIChwb3MueCA8IGRpbXgpICYmIChwb3MueSA+PSAwKSAmJiAocG9zLnkgPCBkaW15KTtcbn07XG5cbmV4cG9ydCBkZWZhdWx0IGNsYXNzIE1hcCB7XG5cbiAgLyoqXG4gICogQHBhcmFtIHtBcnJheVtdfSBtYXRyaXggTWF0cml4IFRhYmxlLCBkZWZhdWx0cyB0byBbWzBdXSwgaWYgYXJndW1lbnQgaXMgbm90IHByb3ZpZGVkXG4gICogQHBhcmFtIHtBcnJheX0gbWF0cml4W10gQSByb3cgd2l0aGluIHRoZSBtYXRyaXggdGFibGVcbiAgKiBAcGFyYW0ge251bWJlcn0gbWF0cml4W11bXSBhIGNlbGwgd2l0aGluIGEgcm93LCByZXByZXNlbnRlZCBieSBlaXRoZXIgMSBvciAwXG4gICovXG4gIGNvbnN0cnVjdG9yKG1hdHJpeCA9IFtbMF1dKSB7XG4gICAgdGhpcy5fZGlteCA9IG1hdHJpeFswXS5sZW5ndGg7XG4gICAgdGhpcy5fZGlteSA9IG1hdHJpeC5sZW5ndGg7XG4gICAgdGhpcy5fcG9pbnRzID0gX2ZsYXR0ZW4obWF0cml4KTtcbiAgfVxuXG4gIC8qKlxuICAgKiBwb2ludHMgZ2V0dGVyXG4gICAqIEByZXR1cm4ge0FycmF5W2Jvb2xlYW5dfVxuICAgKi9cbiAgZ2V0IHBvaW50cygpIHtcbiAgICByZXR1cm4gdGhpcy5fcG9pbnRzO1xuICB9XG5cbiAgc2V0IHBvaW50cyhtYXRyaXgpIHtcbiAgICB0aGlzLl9wb2ludHMgPSBfZmxhdHRlbihtYXRyaXgpO1xuICB9XG5cbiAgZmxhdHRlbihtYXRyaXgpIHtcbiAgICByZXR1cm4gX2ZsYXR0ZW4obWF0cml4KTtcbiAgfVxuICAvKipcbiAgICogQHBhcmFtIHtudW1iZXJ9IHBvc1xuICAgKiBAcGFyYW0ge2Jvb2xlYW59IHN0aWNreVxuICAgKi9cbiAgZ2V0UG9pbnQocG9zLCBzdGlja3kpIHtcbiAgICBsZXQgeCA9IHBvcy54LFxuICAgICAgeSA9IHBvcy55O1xuXG4gICAgaWYgKCF2YWxpZFBvaW50KHBvcywgdGhpcy5fZGlteCwgdGhpcy5fZGlteSkpIHtcbiAgICAgIHJldHVybiBmYWxzZTtcbiAgICB9IGVsc2Uge1xuICAgICAgbGV0IHIgPSB0aGlzLl9wb2ludHNbeCAqIHRoaXMuX2RpbXggKyB5XTtcblxuICAgICAgdGhpcy5zZXRQb2ludChwb3MsIHN0aWNreSk7XG5cbiAgICAgIHJldHVybiByO1xuICAgIH1cbiAgfVxuXG4gIC8qKlxuICAgKiBAcGFyYW0geyp9IHBvc1xuICAgKiBAcGFyYW0geyp9IHZhbHVlXG4gICAqL1xuICBzZXRQb2ludChwb3MsIHZhbHVlKSB7XG4gICAgdmFyIHggPSBwb3MueCxcbiAgICAgIHkgPSBwb3MueTtcblxuICAgIGlmICh0eXBlb2YgdmFsdWUgIT09ICd1bmRlZmluZWQnKSB0aGlzLl9wb2ludHNbeCAqIHRoaXMuX2RpbXggKyB5XSA9IHZhbHVlO1xuXG4gICAgcmV0dXJuIHRoaXMuX3BvaW50c1t4ICogdGhpcy5fZGlteCArIHldO1xuICB9XG5cbn1cbiIsImltcG9ydCBNYXAgZnJvbSAnLi9tYXAuanMnO1xuXG5sZXQgUG9pbnQgPSAoeCwgeSkgPT4ge1xuICByZXR1cm4ge1xuICAgIHg6IHgsXG4gICAgeTogeVxuICB9O1xufTtcblxubGV0IGdldEZ1dHVyZXMgPSAoY3JydHMsIG1hcCkgPT4ge1xuXG4gIGxldCBuZXh0cyA9IFtdO1xuXG4gIGNvbnN0IGdldEFkamFjZW50cyA9IChwb3MpID0+IHtcbiAgICBjb25zdCB4ID0gcG9zLngsXG4gICAgICB5ID0gcG9zLnk7XG5cbiAgICByZXR1cm4gW1xuICAgICAgUG9pbnQoeCAtIDEsIHkpLCBQb2ludCh4ICsgMSwgeSksXG4gICAgICBQb2ludCh4LCB5IC0gMSksIFBvaW50KHgsIHkgKyAxKVxuICAgIF07XG4gIH07XG5cbiAgZm9yIChsZXQgaSA9IDA7IGkgPCBjcnJ0cy5sZW5ndGg7IGkrKykge1xuICAgIGxldCBhZGpzID0gZ2V0QWRqYWNlbnRzKGNycnRzW2ldKTtcblxuICAgIGZvciAobGV0IGogPSAwOyBqIDwgYWRqcy5sZW5ndGg7IGorKykge1xuICAgICAgaWYgKG1hcC5nZXRQb2ludChhZGpzW2pdLCBmYWxzZSkpIHtcbiAgICAgICAgbmV4dHMucHVzaChhZGpzW2pdKTtcbiAgICAgIH1cbiAgICB9XG4gIH1cbiAgcmV0dXJuIG5leHRzO1xufTtcblxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgUGF0aCBleHRlbmRzIE1hcCB7XG5cbiAgLyoqXG4gICAqIEBwYXJhbSB7QXJyYXlbXX0gbWF0cml4IE1hdHJpeCBUYWJsZSwgZGVmYXVsdHMgdG8gW1swXV0sIGlmIGFyZ3VtZW50IGlzIG5vdCBwcm92aWRlZFxuICAgKiBAcGFyYW0ge251bWJlcltdfSBtYXRyaXhbXSBBIHJvdyB3aXRoaW4gdGhlIG1hdHJpeCB0YWJsZVxuICAgKiBAcGFyYW0ge251bWJlcn0gbWF0cml4W11bXSBhIGNlbGwgd2l0aGluIGEgcm93LCByZXByZXNlbnRlZCBieSBlaXRoZXIgMSBvciAwXG4gICAqL1xuICBjb25zdHJ1Y3RvcihtYXRyaXggPSBbWzBdXSkge1xuICAgIHN1cGVyKG1hdHJpeCk7XG5cbiAgICB0aGlzLl9zdGFydCA9IHtcbiAgICAgIHg6IDAsXG4gICAgICB5OiAwXG4gICAgfTtcbiAgICB0aGlzLl9nb2FsID0ge1xuICAgICAgeDogbWF0cml4WzBdLmxlbmd0aCAtIDEsXG4gICAgICB5OiBtYXRyaXgubGVuZ3RoIC0gMVxuICAgIH07XG4gIH1cblxuICAvKipcbiAgICogR2V0dGVyc1xuICAgKi9cbiAgZ2V0IHN0YXJ0KCkge1xuICAgIHJldHVybiB0aGlzLl9zdGFydDtcbiAgfVxuXG4gIGdldCBnb2FsKCkge1xuICAgIHJldHVybiB0aGlzLl9nb2FsO1xuICB9XG5cbiAgLyoqXG4gICAqIEByZXR1cm4ge2Jvb2xlYW59XG4gICAqL1xuICBleGlzdHMoKSB7XG4gICAgbGV0IGZ1dHVyZXMgPSBbdGhpcy5zdGFydF07XG5cbiAgICBkbyB7XG4gICAgICBmdXR1cmVzID0gZ2V0RnV0dXJlcyhmdXR1cmVzLCB0aGlzKTtcblxuICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPCBmdXR1cmVzLmxlbmd0aDsgaSsrKSB7XG4gICAgICAgIGxldCBmdXR1cmUgPSBmdXR1cmVzW2ldO1xuXG4gICAgICAgIGlmICgoZnV0dXJlLnggPT09IHRoaXMuX2dvYWwueCkgJiYgKGZ1dHVyZS55ID09PSB0aGlzLl9nb2FsLnkpKSByZXR1cm4gdHJ1ZTtcbiAgICAgIH1cbiAgICB9IHdoaWxlIChmdXR1cmVzLmxlbmd0aCAhPT0gMCk7XG4gICAgcmV0dXJuIGZhbHNlO1xuICB9XG5cbn1cbiJdLCJzb3VyY2VSb290IjoiIn0=