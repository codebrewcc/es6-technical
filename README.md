# ES6 library for a labyrinth path finder & a split function for a string representing a number

## Features

* Webpack 4 based.
* ES6 as a source.
* ES6 test setup with [Mocha](http://mochajs.org/) and [Chai](http://chaijs.com/).
* Linting with [ESLint](http://eslint.org/).

## Requirements
For development, you will only need Node.js on your environment.

### Node

[Node](http://nodejs.org/) is really easy to install
You should be able to run the following commands after the installation procedure
below.

    $ node --version
    v8.11.2

    $ npm --version
    v5.6.0

## Getting started

1. Clone
* $ `git clone https://iestyn02@bitbucket.org/axierio/es6-technical.git`

2. Build
  * $ `cd moovi`
  * $ `npm install`
  * $ `npm run build`

3. Tests
  * $ `npm run test`

## Scripts

* `npm build` or `npm run build` - produces production version of your library under the `lib` folder
* `npm run dev` - produces development version of your library and runs a watcher
* `npm run test` - it runs the tests :)
* `npm run test:watch` - same as above but in a watch mode

## Build & Serve

* $ cd es6-technical
* $ npm run build
* $ library is now bundled, you can import the module like this `const lib = require('../lib/field-aware-technical');`

## Demo

* $ cd es6-technical/demo
* $ `nodemon example.js` or `node example.js`
* $ output will be in the console

## License

MIT