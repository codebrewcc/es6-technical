const lib = require('../lib/field-aware-technical');

let exp = new lib.Exp();

/**
* @param {String} n - A non-negative string representing a number where 1 <= n.length <= 13
* @return {Array.String}
*/

let expFn = (n) => {
    exp.setValue(n);
    console.info(exp.transform());
}

let labyrinthFn = (matrix) => {
    path = new lib.Path(matrix);
    console.info(path.exists());
}

let runExpTests = () => {
    expFn('7970521.5544');
    expFn('7496314');
    expFn('0');
}

runExpTests();

let runLabyrinthTests = () => {
    labyrinthFn([[0, 0, 0, 0, 0], [1, 1, 1, 1, 0], [0, 0, 0, 0, 0], [0, 1, 1, 1, 1], [0, 0, 0, 0, 0]]);
    labyrinthFn([[0, 1, 1, 1, 1], [1, 1, 1, 1, 0], [0, 0, 0, 0, 0], [0, 1, 1, 1, 1], [0, 0, 0, 0, 0]]);
}

runLabyrinthTests();