/* global describe, it, before */

import chai from 'chai';
import { Exp, Map, Path } from '../lib/field-aware-technical';

chai.expect();

const expect = chai.expect;
const matrixPathExists = [[0, 0, 0, 0, 0], [0, 0, 0, 0, 0], [0, 0, 0, 0, 0], [0, 0, 0, 0, 0], [0, 0, 0, 0, 0]];
const matrixPathDoesntExist = [[0, 1, 1, 1, 1], [1, 1, 1, 1, 0], [0, 0, 0, 0, 0], [0, 1, 1, 1, 1], [0, 0, 0, 0, 0]];

let exp, map, path;

describe('Given an instance of Exp class with no arguments passed', () => {
  before(() => {
    exp = new Exp();
  });
  describe('when I retrieve the value from Exp transform()', () => {
    it('the return value should be []', () => {
      expect(exp.transform()).to.deep.be.equal([]);
    });
  });
});

describe('Given an instance of Exp class with "7970521.5544" passed', () => {
  before(() => {
    exp = new Exp('7970521.5544');
  });
  describe('when I retrieve the value from Exp transform()', () => {
    it('the return value should be ["7000000", "900000", "70000", "500", "20", "1", ".5", ".05", ".004", ".0004"]', () => {
      expect(exp.transform()).to.deep.be.equal(["7000000", "900000", "70000", "500", "20", "1", ".5", ".05", ".004", ".0004"]);
    });
  });
});

describe('Given an instance of Exp class with "7496314" passed', () => {
  before(() => {
    exp = new Exp('7496314');
  });
  describe('when I retrieve the value from Exp transform()', () => {
    it('the return value should be ["7000000", "400000", "90000", "6000", "300", "10", "4"]', () => {
      expect(exp.transform()).to.deep.be.equal(["7000000", "400000", "90000", "6000", "300", "10", "4"]);
    });
  });
});

describe('Given an instance of Map class with no arguments passed', () => {
  before(() => {
    map = new Map();
  });
  describe('when I retrieve the Map instance points', () => {
    it('the return value should be [true]', () => {
      expect(map.points).to.deep.be.equal([true]);
    });
  });
});

describe('Given an instance of Path with no arguments passed', () => {
  before(() => {
    path = new Path();
  });
  describe('when I need the Path instance coordinates', () => {
    it('the start coordinates should be { x: 0, y: 0 }', () => {
      expect(path.start).to.deep.be.equal({ x: 0, y: 0 });
    });
    it('the goal coordinates should be { x: 0, y: 0 }', () => {
      expect(path.goal).to.deep.be.equal({ x: 0, y: 0 });
    });
  });
  describe('when I need to check if a possible path exists', () => {
    it('path shouldn\'t exist', () => {
      expect(path.exists()).to.be.equal(false);
    });
  });
});

describe('Given an instance of Path with a valid 5 x 5 matrix, passed as an argument', () => {
  before(() => {
    path = new Path(matrixPathExists);
  });
  describe('when I need the Path instance coordinates', () => {
    it('the start coordinates need to be { x: 0, y: 0 }', () => {
      expect(path.start).to.deep.be.equal({ x: 0, y: 0 });
    });
    it('the goal coordinates need to be { x: 4, y: 4 }', () => {
      expect(path.goal).to.deep.be.equal({ x: 4, y: 4 });
    });
  });
});

describe('Given an instance of Path with a valid 5 x 5 matrix and a possible path, passed as an argument', () => {
  before(() => {
    path = new Path(matrixPathExists);
  });
  describe('when I need to check if a possible path exists', () => {
    it('path should exist', () => {
      expect(path.exists()).to.be.equal(true);
    });
  });
});

describe('Given an instance of Path with a valid 5 x 5 matrix and doesn\'t have a possible path, passed as an argument', () => {
  before(() => {
    path = new Path(matrixPathDoesntExist);
  });
  describe('when I need to check if a possible path exists', () => {
    it('path shouldn\'t exist', () => {
      expect(path.exists()).to.be.equal(false);
    });
  });
});

