/* eslint-disable no-else-return */

let pad = (num, size, floating) => {
  let s = num + '';

  if (floating) {
    while (s.length < size) s = '0' + s;
  } else {
    while (s.length < size) s = s + '0';
  };
  return floating ? '.' + s : s;
};

export default class Exp {

  /**
   * @param {string} n - A non-negative string representing a number where 1 <= n.length <= 13
   */
  constructor(n = '') {
    this._n = '' + n;
  }

  /**
   * @return {string[]}
   */
  transform() {
    if (parseFloat(this._n)) {
      let ret = this._n.split('');
      let toRemove = [];
      const floatIndex = ret.indexOf('.');
      const paddingIndex = floatIndex !== -1 ? floatIndex : ret.length;

      if (floatIndex) {
        ret.splice(paddingIndex, 1);
      };

      ret.forEach((val, i) => {
        if (parseFloat(ret[i])) {
          ret[i] = i >= paddingIndex ? pad(val, ((i + 1) - paddingIndex), true) : pad(val, (paddingIndex - i));
        } else {
          toRemove.push(i);
        }
      });

      toRemove.forEach((val) => {
        ret.splice(val, 1);
      });

      return ret;
    } else {
      return [];
    }
  }

  get val() {
    return this._n;
  }

  setValue(val) {
    this._n = '' + val;
  }

}
