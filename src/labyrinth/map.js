/* eslint-disable no-else-return */

let _flatten = (matrix) => {
  let pts = [];

  for (let y = 0; y < matrix.length; y++) {
    for (let x = 0; x < matrix[y].length; x++) {
      pts.push(!matrix[y][x]);
    }
  }
  return pts;
};

let validPoint = (pos, dimx, dimy) => {
  return (pos.x >= 0) && (pos.x < dimx) && (pos.y >= 0) && (pos.y < dimy);
};

export default class Map {

  /**
  * @param {Array[]} matrix Matrix Table, defaults to [[0]], if argument is not provided
  * @param {Array} matrix[] A row within the matrix table
  * @param {number} matrix[][] a cell within a row, represented by either 1 or 0
  */
  constructor(matrix = [[0]]) {
    this._dimx = matrix[0].length;
    this._dimy = matrix.length;
    this._points = _flatten(matrix);
  }

  /**
   * points getter
   * @return {Array[boolean]}
   */
  get points() {
    return this._points;
  }

  set points(matrix) {
    this._points = _flatten(matrix);
  }

  flatten(matrix) {
    return _flatten(matrix);
  }
  /**
   * @param {number} pos
   * @param {boolean} sticky
   */
  getPoint(pos, sticky) {
    let x = pos.x,
      y = pos.y;

    if (!validPoint(pos, this._dimx, this._dimy)) {
      return false;
    } else {
      let r = this._points[x * this._dimx + y];

      this.setPoint(pos, sticky);

      return r;
    }
  }

  /**
   * @param {*} pos
   * @param {*} value
   */
  setPoint(pos, value) {
    var x = pos.x,
      y = pos.y;

    if (typeof value !== 'undefined') this._points[x * this._dimx + y] = value;

    return this._points[x * this._dimx + y];
  }

}
