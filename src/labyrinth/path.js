import Map from './map.js';

let Point = (x, y) => {
  return {
    x: x,
    y: y
  };
};

let getFutures = (crrts, map) => {

  let nexts = [];

  const getAdjacents = (pos) => {
    const x = pos.x,
      y = pos.y;

    return [
      Point(x - 1, y), Point(x + 1, y),
      Point(x, y - 1), Point(x, y + 1)
    ];
  };

  for (let i = 0; i < crrts.length; i++) {
    let adjs = getAdjacents(crrts[i]);

    for (let j = 0; j < adjs.length; j++) {
      if (map.getPoint(adjs[j], false)) {
        nexts.push(adjs[j]);
      }
    }
  }
  return nexts;
};

export default class Path extends Map {

  /**
   * @param {Array[]} matrix Matrix Table, defaults to [[0]], if argument is not provided
   * @param {number[]} matrix[] A row within the matrix table
   * @param {number} matrix[][] a cell within a row, represented by either 1 or 0
   */
  constructor(matrix = [[0]]) {
    super(matrix);

    this._start = {
      x: 0,
      y: 0
    };
    this._goal = {
      x: matrix[0].length - 1,
      y: matrix.length - 1
    };
  }

  /**
   * Getters
   */
  get start() {
    return this._start;
  }

  get goal() {
    return this._goal;
  }

  /**
   * @return {boolean}
   */
  exists() {
    let futures = [this.start];

    do {
      futures = getFutures(futures, this);

      for (let i = 0; i < futures.length; i++) {
        let future = futures[i];

        if ((future.x === this._goal.x) && (future.y === this._goal.y)) return true;
      }
    } while (futures.length !== 0);
    return false;
  }

}
